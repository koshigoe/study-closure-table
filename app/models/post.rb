class Post < ActiveRecord::Base
  belongs_to :category
  has_many :category_tree_paths, foreign_key: 'descendant_id', primary_key: 'category_id'

  validates :title, presence: true
  validates :content, presence: true

  def self.categorized_as(category)
    joins(:category_tree_paths)
      .where(category_tree_paths: { ancestor_id: category })
  end
end

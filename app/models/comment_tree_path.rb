class CommentTreePath < ActiveRecord::Base
  self.primary_keys = [:ancestor_id, :descendant_id]

  belongs_to :ancestor,   class_name: 'Comment', foreign_key: :ancestor_id
  belongs_to :descendant, class_name: 'Comment', foreign_key: :descendant_id

  validates :ancestor_id, presence: true
  validates :descendant_id, presence: true
end

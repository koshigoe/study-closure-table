# -*- coding: utf-8 -*-
class Category < ActiveRecord::Base
  belongs_to :parent, foreign_key: 'parent_id', class_name: name
  has_many :children, foreign_key: 'parent_id', class_name: name

  has_many :ancestor_tree_paths, class_name: "#{name}TreePath", foreign_key: 'descendant_id'
  has_many :ancestors, through: :ancestor_tree_paths, source: 'ancestor'

  has_many :descendant_tree_paths, class_name: "#{name}TreePath", foreign_key: 'ancestor_id'
  has_many :descendants, through: :descendant_tree_paths, source: 'descendant'

  before_validation :disallow_move_inside, on: :update, if: ->(x){ x.parent_id_changed? }

  validates :name, presence: true, uniqueness: true

  after_create :create_paths
  after_update :move_tree, if: ->(x){ x.parent_id_changed? }

  scope :named, ->(name) { where(name: name) }

  def distance_to(descendant)
    connection.select_value <<SQL
SELECT COUNT(*)
  FROM #{self.class.table_name.singularize}_tree_paths
 WHERE
   descendant_id = #{descendant.id}
   AND ancestor_id IN (
       SELECT descendant_id
         FROM #{self.class.table_name.singularize}_tree_paths
        WHERE ancestor_id = #{id}
          AND descendant_id != #{id}
   )
SQL
  end

  protected

  # 追加したノードへのパスを作る
  #
  # * 自分と先祖へのパスを作る
  #
  def create_paths
    connection.execute <<SQL
INSERT INTO #{self.class.table_name.singularize}_tree_paths (ancestor_id, descendant_id)
  SELECT t.ancestor_id, #{id}
    FROM #{self.class.table_name.singularize}_tree_paths AS t
   WHERE t.descendant_id = #{parent.try(:id) || 'NULL'}
UNION ALL
  SELECT #{id}, #{id}
SQL
  end

  # サブツリー内への移動は許可しない
  #
  def disallow_move_inside
    if parent_id.in? descendants.pluck(self.class.primary_key)
      errors.add(:parent_id, 'can not move inside subtree')
    end
  end

  # 指定した親の子供に移動させる
  #
  # * サブツリー内のパスは維持したまま、サブツリー内のノードへの余計な参照を切る
  # * CROSS JOIN を用いたデカルト積で、移動先ノードを子孫とするノードからの参照を作る
  #
  def move_tree
    connection.execute <<SQL
DELETE FROM #{self.class.table_name.singularize}_tree_paths
 WHERE descendant_id IN (SELECT x.id FROM (SELECT descendant_id AS id
                                             FROM #{self.class.table_name.singularize}_tree_paths
                                            WHERE ancestor_id = #{id}) AS x)
   AND ancestor_id IN (SELECT y.id FROM (SELECT ancestor_id AS id
                                           FROM #{self.class.table_name.singularize}_tree_paths
                                          WHERE descendant_id = #{id}
                                            AND ancestor_id != descendant_id) AS y)
SQL

    connection.execute <<SQL
INSERT INTO #{self.class.table_name.singularize}_tree_paths (ancestor_id, descendant_id)
  SELECT supertree.ancestor_id, subtree.descendant_id
    FROM #{self.class.table_name.singularize}_tree_paths AS supertree
      CROSS JOIN #{self.class.table_name.singularize}_tree_paths AS subtree
   WHERE supertree.descendant_id = #{parent.id}
     AND subtree.ancestor_id = #{id}
SQL
  end
end

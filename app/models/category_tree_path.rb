class CategoryTreePath < ActiveRecord::Base
  self.primary_keys = [:ancestor_id, :descendant_id]

  belongs_to :ancestor,   class_name: 'Category', foreign_key: :ancestor_id
  belongs_to :descendant, class_name: 'Category', foreign_key: :descendant_id

  validates :ancestor_id, presence: true
  validates :descendant_id, presence: true
end

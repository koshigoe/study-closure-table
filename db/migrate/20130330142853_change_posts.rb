class ChangePosts < ActiveRecord::Migration
  def change
    change_table :posts do |t|
      t.belongs_to :category, after: :post_id
      t.foreign_key :categories, primary_key: :category_id, dependent: :delete
    end
  end
end

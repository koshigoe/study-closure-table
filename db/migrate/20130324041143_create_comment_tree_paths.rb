class CreateCommentTreePaths < ActiveRecord::Migration
  def change
    create_table :comment_tree_paths, id: false do |t|
      t.integer :ancestor_id, null: false
      t.integer :descendant_id, null: false
      t.integer :distance, null: false, default: 0

      t.foreign_key :comments, primary_key: :comment_id, column: :ancestor_id, dependent: :delete
      t.foreign_key :comments, primary_key: :comment_id, column: :descendant_id, dependent: :delete
    end
    add_index :comment_tree_paths, [:ancestor_id, :descendant_id], name: 'unique', unique: true
  end
end

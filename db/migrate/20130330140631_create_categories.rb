class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories, primary_key: :category_id do |t|
      t.references :parent
      t.string :name, null: false

      t.timestamps

      t.foreign_key :categories, primary_key: :category_id, column: :parent_id, dependent: :delete
    end
    add_index :categories, [:name], unique: true, name: 'unique_name'
  end
end

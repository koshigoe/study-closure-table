class CreateCategoryTreePaths < ActiveRecord::Migration
  def change
    create_table :category_tree_paths, id: false do |t|
      t.integer :ancestor_id, null: false
      t.integer :descendant_id, null: false
      t.integer :distance, null: false, default: 0

      t.foreign_key :categories, primary_key: :category_id, column: :ancestor_id, dependent: :delete
      t.foreign_key :categories, primary_key: :category_id, column: :descendant_id, dependent: :delete
    end
    add_index :category_tree_paths, [:ancestor_id, :descendant_id], name: 'unique', unique: true
  end
end

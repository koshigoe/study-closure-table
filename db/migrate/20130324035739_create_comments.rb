class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments, primary_key: :comment_id do |t|
      t.belongs_to :post, null: false
      t.references :parent
      t.text :content

      t.timestamps

      t.foreign_key :posts, primary_key: :post_id, dependent: :delete
      t.foreign_key :comments, primary_key: :comment_id, column: :parent_id, dependent: :delete
    end
  end
end

# -*- coding: utf-8 -*-
require 'spec_helper'

describe Post do
  #
  #       ( 1 )
  #      /    \
  #     (2)   (3)
  #    /   \
  #  (4)   (5)
  #
  let(:category_1) { Category.create! name: '1' }
  let(:category_2) { category_1.children.create! name: '2' }
  let(:category_3) { category_1.children.create! name: '3' }
  let(:category_4) { category_2.children.create! name: '4' }
  let(:category_5) { category_2.children.create! name: '5' }

  describe 'validation' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:content) }
  end

  describe '.categorized_as' do
    before do
      described_class.create! title: 'no-category 1', content: 'no-category 1'
      described_class.create! title: 'no-category 2', content: 'no-category 2'
      described_class.create! title: 'no-category 3', content: 'no-category 3'
      described_class.create! title: 'c1 1', content: 'c1 1', category: category_1
      described_class.create! title: 'c1 2', content: 'c1 2', category: category_1
      described_class.create! title: 'c2 1', content: 'c2 1', category: category_2
      described_class.create! title: 'c2 2', content: 'c2 2', category: category_2
      described_class.create! title: 'c3 1', content: 'c3 1', category: category_3
      described_class.create! title: 'c3 2', content: 'c3 2', category: category_3
      described_class.create! title: 'c4 1', content: 'c4 1', category: category_4
      described_class.create! title: 'c4 2', content: 'c4 2', category: category_4
      described_class.create! title: 'c5 1', content: 'c5 1', category: category_5
      described_class.create! title: 'c5 2', content: 'c5 2', category: category_5
    end
    it 'カテゴリで絞り込んだ結果を得られること' do
      described_class.categorized_as(category_1).pluck(:title).should == ['c1 1', 'c1 2', 'c2 1', 'c2 2', 'c3 1', 'c3 2', 'c4 1', 'c4 2', 'c5 1', 'c5 2']
      described_class.categorized_as(category_2).pluck(:title).should == ['c2 1', 'c2 2', 'c4 1', 'c4 2', 'c5 1', 'c5 2']
      described_class.categorized_as(category_3).pluck(:title).should == ['c3 1', 'c3 2']
      described_class.categorized_as(category_4).pluck(:title).should == ['c4 1', 'c4 2']
      described_class.categorized_as(category_5).pluck(:title).should == ['c5 1', 'c5 2']
    end
  end
end

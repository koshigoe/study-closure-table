# -*- coding: utf-8 -*-
require 'spec_helper'

describe Comment do
  let(:post) { create :valid_post  }
  let(:other_post) { create :other_post  }
  #
  #       root
  #      /    \
  #     (1)   (2)
  #    /   \
  #  (3)   (4)
  #        /
  #      (5) new
  #
  let(:root) { create :valid_comment, post: post }
  let(:node1) { root.children.create! post: post, content: '1' }
  let(:node2) { root.children.create! post: post, content: '2' }
  let(:node3) { node1.children.create! post: post, content: '3' }
  let(:node4) { node1.children.create! post: post, content: '4' }

  describe 'validation' do
    it { should validate_presence_of(:post_id) }
    it { should validate_presence_of(:content) }

    context '親子で Post が異なる場合' do
      subject { root.children.build post: other_post, content: '1' }
      it { should be_invalid }
    end

    context 'サブツリー内のノード以下に移動しようとした場合' do
      before { [root, node1, node2, node3, node4] }
      it 'バリデーションエラーになること' do
        node1.update_attributes(parent_id: node3.id).should be_false
        node1.should be_invalid
      end
    end
  end

  describe 'relation' do
    it { should belong_to(:post) }
    it { should belong_to(:parent) }
    it { should have_many(:children) }
    it { should have_many(:ancestor_tree_paths) }
    it { should have_many(:ancestors) }
    it { should have_many(:descendant_tree_paths) }
    it { should have_many(:descendants) }
  end

  describe '.create!' do
    context '親無し' do
      it '自分自身へのパスを作成すること' do
        expect {
          comment = create :valid_comment
          CommentTreePath.last.ancestor.should == comment
          CommentTreePath.last.descendant.should == comment
        }.to change(CommentTreePath, :count).by(1)
      end
    end

    context '親有り' do
      before { [root, node1, node2, node3, node4] }

      it 'パスを作成すること' do
        expect {
          node5 = node4.children.create! post: post, content: '5'
          node5.ancestors.should have(4).items
          node5.ancestors.should == [root, node1, node4, node5]
          node5.descendants.should == [node5]
          node1.descendants.should == [node1, node3, node4, node5]
          node3.descendants.should == [node3]
          node4.descendants.should == [node4, node5]
        }.to change(CommentTreePath, :count).by(4)
      end
    end
  end

  describe '#save' do
    before { [root, node1, node2, node3, node4] }

    context '親を変更しない場合' do
      it 'サブツリーを移動させないこと' do
        node1.should_not_receive(:move_tree)
        node1.update_attributes content: 'hoge'
      end
    end

    context '親を変更した場合' do
      it 'サブツリーを移動させること' do
        expect {
          node1.update_attributes parent_id: node2.id
        }.to change(CommentTreePath, :count).from(11).to(14)
        root.descendants.should == [root, node1, node2, node3, node4]
        node2.descendants.should == [node1, node2, node3, node4]
        node1.descendants.should == [node1, node3, node4]
        node3.descendants.should == [node3]
        node4.descendants.should == [node4]
      end
    end
  end

  describe '#destroy' do
    before { [root, node1, node2, node3, node4] }

    it '子孫すべてと関係するパスをすべて削除すること' do
      expect {
        expect {
          node1.destroy
        }.to change(described_class, :count).by(-3)
      }.to change(CommentTreePath, :count).by(-8)
      CommentTreePath.count.should be(3)
      CommentTreePath.where(ancestor_id: root.id).should have(2).items
      CommentTreePath.where(ancestor_id: node2.id).should have(1).items
    end
  end

  describe '#distance_to' do
    before { [root, node1, node2, node3, node4] }

    context '子孫の場合' do
      it 'ノード間の距離を計算できること' do
        root.distance_to(root).should be(0)
        root.distance_to(node1).should be(1)
        root.distance_to(node2).should be(1)
        root.distance_to(node3).should be(2)
        root.distance_to(node4).should be(2)
        node1.distance_to(node1).should be(0)
        node1.distance_to(node3).should be(1)
        node1.distance_to(node4).should be(1)
      end
    end

    context '子孫じゃない場合' do
      pending
    end
  end
end

# -*- coding: utf-8 -*-
require 'spec_helper'

describe CommentTreePath do
  describe 'validation' do
    it { should validate_presence_of(:ancestor_id) }
    it { should validate_presence_of(:descendant_id) }
  end

  describe 'relation' do
    it { should belong_to(:ancestor) }
    it { should belong_to(:descendant) }
  end
end

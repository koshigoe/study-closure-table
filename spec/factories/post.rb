FactoryGirl.define do
  factory :valid_post, class: Post do
    title 'TITLE'
    content 'CONTENT'
  end

  factory :other_post, class: Post do
    title 'TITLE'
    content 'CONTENT'
  end
end

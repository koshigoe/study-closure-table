FactoryGirl.define do
  factory :valid_comment, class: Comment do
    association :post, factory: :valid_post
    content 'CONTENT'
  end
end
